package utils.reporting

import play.api.libs.json._

object DbHelper {
  def listMapToJson(data: List[Map[String, Any]]) = {
    data.map(this.mapToJson).toSeq
  }

  def mapToJson(data: Map[String, Any]) = {
    JsObject(
      data.map {
        case (k, v: Int) => k -> JsNumber(v)
        case (k, v: Long) => k -> JsNumber(v)
        case (k, v: Double) => k -> JsNumber(v)
        case (k, v: java.math.BigDecimal) => k -> JsNumber(v.doubleValue())
        case (k, v: Float) => k -> JsNumber(v)
        case (k, v: String) => k -> JsString(v)
        case (k, v: Boolean) => k -> JsBoolean(v)
        case (k, Some(v: Int)) => k -> JsNumber(v)
        case (k, Some(v: Long)) => k -> JsNumber(v)
        case (k, Some(v: Double)) => k -> JsNumber(v)
        case (k, Some(v: java.math.BigDecimal)) => k -> JsNumber(v)
        case (k, Some(v: Float)) => k -> JsNumber(v)
        case (k, Some(v: String)) => k -> JsString(v)
        case (k, Some(v: Boolean)) => k -> JsBoolean(v)
        case (k, None) => k -> JsNull
        case (k, Some(v)) => k -> JsString(v.toString)
        case (k, v) => k -> JsString(v.toString)
      }.toSeq
    )
  }

  def listMapToCsv(data: List[Map[String, Any]]) = {
    data.map(this.mapToCsv).toSeq
  }

  def mapToCsv(data: Map[String, Any]) = {
    data.map {
      case (k, v: Int) => k -> v
      case (k, v: Long) => k -> v
      case (k, v: Double) => k -> v
      case (k, v: java.math.BigDecimal) => k -> v
      case (k, v: Float) => k -> v
      case (k, v: String) => k -> v
      case (k, v: Boolean) => k -> v
      case (k, Some(v: Int)) => k -> v
      case (k, Some(v: Long)) => k -> v
      case (k, Some(v: Double)) => k -> v
      case (k, Some(v: java.math.BigDecimal)) => k -> v
      case (k, Some(v: Float)) => k -> v
      case (k, Some(v: String)) => k -> v
      case (k, Some(v: Boolean)) => k -> v
      case (k, None) => k -> null
      case (k, Some(v)) => k -> JsString(v.toString)
      case (k, v) => k -> v.toString
    }
  }

  def mapToValue(data: Map[String, Any]) = {
    data.map {
      case (k, v: Int) => JsNumber(v)
      case (k, v: Long) => JsNumber(v)
      case (k, v: Double) => JsNumber(v)
      case (k, v: java.math.BigDecimal) => JsNumber(v.doubleValue())
      case (k, v: Float) => JsNumber(v)
      case (k, v: String) => JsString(v)
      case (k, v: Boolean) => JsBoolean(v)
      case (k, Some(v: Int)) => JsNumber(v)
      case (k, Some(v: Long)) => JsNumber(v)
      case (k, Some(v: Double)) => JsNumber(v)
      case (k, Some(v: java.math.BigDecimal)) => JsNumber(v)
      case (k, Some(v: Float)) => JsNumber(v)
      case (k, Some(v: String)) => JsString(v)
      case (k, Some(v: Boolean)) => JsBoolean(v)
      case (k, None) => JsNull
      case (k, Some(v)) => k -> JsString(v.toString)
      case (k, v) => JsString(v.toString)
    }.headOption.getOrElse(JsNull)
  }

}
