package models.reporting.canned

import play.api.db.Database
import anorm._
import anorm.SqlParser._
import play.api.Logger

import scala.language.postfixOps

import models.reporting.{ BaseModel, Page, Pagination }

object ReportRevenueByDay extends BaseModel {

   def run(db: Database): Page[Map[String, Any]] = {
    val startTime = System.currentTimeMillis

    val dataSource = "rollup_banner_cpm_cost"

    val sql: String = s"""
      select rollup.day, placement.id, placement.name, rollup.cpm_rate, rollup.requests, rollup.cost
      from rollup_banner_cpm_cost as rollup
      join placement on placement.id = rollup.publisher_id
      limit 10
    """

    val sqlCounter: String = s"""
      select count(1) as `recordCount`
      from rollup_banner_cpm_cost as rollup
      join placement on placement.id = rollup.publisher_id
    """

    val r: List[Map[String, Any]] = {
      db.withConnection { implicit connection =>
        SQL(
          sql
        ).on(
         "deleted" -> 0
        ).as(this.mapParser *)
      }
    }

    lazy val c: Long = {
      db.withConnection { implicit connection =>
        SQL(
          sqlCounter
        ).on(
         "deleted" -> 0
        ).as(scalar[Long].single)
      }
    }

    val endTime = System.currentTimeMillis
    // add this to the logging object
    val requestTime = endTime - startTime
    Logger.debug("Query execution time: %dms".format(requestTime))

    /*
    ReportLogger.log(
      executionTime = requestTime,
      sql = sql,
      filters = filters,
      dataSource =
//      userId = acl.user.id
    )*/

    Page(
      data = r,
      pagination = Pagination(
//        query = formQuery,
        records = r.size,
        total = c,
        current = 1L,
        limit = 10L
      )
    )

  }

}

// Pattern
// 1. input params (filters)
// 2. generated sql (statement)
// 3. time to execute sql statement
// 4. count of all records in query result set without limit
// 5. paging information (context)
