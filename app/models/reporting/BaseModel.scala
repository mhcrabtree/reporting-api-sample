package models.reporting

import anorm._

trait BaseModel {
  val mapParser: RowParser[Map[String, Any]] = RowParser(rowToMap)
  val periodRegex = """.""".r
  private def rowToMap(row: Row): SqlResult[Map[String, Any]] = Success(row.asMap.map {
    case (k: String, v: Any) => {
      val col = k.slice(0, 1) match {
        case "." => k.slice(1, k.length)
        case _ => k
      }
      col -> v
    }
  })
}
