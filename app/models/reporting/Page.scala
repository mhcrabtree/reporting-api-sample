package models.reporting

import play.api.libs.json._

import scala.math.ceil

case class PageShowing(start: Long, end: Long) {
  lazy val json: JsValue = Json.obj(
    "start" -> start,
    "end" -> end
  )
}

case class Pagination(
    records: Long, // the total number of records in the returned data
    total: Long, // the total number of records possible
    current: Long, // the current page
    limit: Long // the number of records per page
//    query: models.forms.FormQuery
) {
  val offset: Long = (current - 1L) * limit
  val pages: Long = ceil(total.toDouble / limit.toDouble).toLong // the total number of pages
  val currentSafe: Long = Option(current).filter(_ <= total).getOrElse(total)
  val previous: Option[Long] = Option(currentSafe - 1L).filter(_ >= 1L)
  val next: Option[Long] = Option(currentSafe + 1L).filter(_ <= pages)

  def firstN(n: Long): Long = if (this.pages > n) this.pages else n
  def lastN(n: Long): Long = {
    val temp: Long = this.pages - n
    if (temp > 0L && temp > this.firstN(n)) temp else this.pages
  }

  val showing: PageShowing = PageShowing(
    start = if (this.records > 0L) this.offset + 1L else 0L,
    end = if (this.records > 0L) this.offset + this.records else 0L
  )
}

case class Page[A](data: List[A], pagination: Pagination) {
  // helpers
//  def query = pagination.query
  def showing: PageShowing = pagination.showing

  // more helpers
  def total: Long = pagination.total
  def records: Long = pagination.records
  def offset: Long = pagination.offset
  def pages: Long = pagination.pages
  def current: Long = pagination.currentSafe
  def previous: Option[Long] = pagination.previous
  def next: Option[Long] = pagination.next

  def json: JsValue = Json.obj(
    "records" -> pagination.records,
    "total" -> pagination.total,
    "pages" -> pagination.pages,
    "limit" -> pagination.limit,
    "current" -> pagination.currentSafe,
    "previous" -> pagination.previous,
    "next" -> pagination.next,
    "showing" -> pagination.showing.json
  )
}
