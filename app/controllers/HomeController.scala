package controllers

import javax.inject._
//import play.api._
import play.api.mvc._
import play.api.db.Database

import play.api.libs.json._

@Singleton
class HomeController @Inject()(db: Database, cc: ControllerComponents) extends AbstractController(cc) {

  def test() = Action { implicit request: Request[AnyContent] =>
    // include the form

    // have the following return a page object
    val page = models.reporting.canned.ReportRevenueByDay.run(db)

    val data: Seq[JsValue] = page.data.map(d => utils.reporting.DbHelper.mapToJson(d))

    lazy val view = views.txt.list(data, page.json)

    Ok(view).as(JSON)
  }

}
